package ru.smirnov.learning;


public class Collatz {

    public static void main(String[] args) {
        int maxLength = 0;
        int forNumber = 0;
        for (int i = 1; i <= 1_000_000; i++) {
            if (i % 1000 == 0) {
                System.out.println(i);
            }
            int length = countSeqLength(i);
            if (length > maxLength) {
                maxLength = length;
                forNumber = i;
            }
        }
        System.out.println("Max length = " + maxLength + " for number " + forNumber);
    }

    static int countSeqLength(final int n) {
        int counter = 1;
        long current = n;
        while (current != 1) {
            System.out.println("current: " + current);
            counter++;
            if (current % 2 == 0) {
                current /= 2;
            } else {
                current = 3 * current + 1;
            }
        }
        return counter;
    }

}
