package ru.smirnov.learning;

import org.junit.Assert;
import org.junit.Test;

public class NumberSeqTest {

    @Test
    public void test() {
        String actual = NumberSeq.expand("1,2,4-7,18-21");
        Assert.assertEquals("1,2,4,5,6,7,18,19,20,21", actual);
    }

}
