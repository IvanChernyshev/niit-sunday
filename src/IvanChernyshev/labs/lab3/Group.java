
import java.util.*;

public class Group {
	private String title;
	private ArrayList<Student> students;
	private int num;
	private Student head;

	public Group(String title) {
		this.title = title;
		this.students = new ArrayList<Student>();
	}

	public String getTitle() {
		return title;
	}

	public ArrayList<Student> getStudents() {
		return students;
	}

	public int getNum() {
		return num;
	}

	private void numberOfNum() {
		this.num = students.size();
	}

	public Student getHead() {
		return head;
	}

	public void setHead(Student head) {
		this.head = head;
	}

	public void addStudent(Student student) {
		this.students.add(student);
		numberOfNum();
	}

	public Student searchStudent(int ID) {
		Student student = null;
		for (Student s : students) {
			if (s.getID() == ID) {
				student = s;
				break;
			}
		}
		return student;
	}

	public Student searchStudent(String fio) {
		Student student = null;
		for (Student s : students) {
			if (s.getFio().equals(fio)) {
				student = s;
				break;
			}
		}
		return student;
	}

	public double averageMarkCalc() {
		double averageMark = 0;
		for (Student s : students) {
			averageMark = averageMark + s.averageMarkCalc();
		}
		return averageMark / students.size();
	}

	public void removeStudent(Student student) {
		this.students.remove(student);
		numberOfNum();
	}

	public void removeStudent(int student) {
		if (student < students.size()) {
			this.students.remove(student);
			numberOfNum();
		}
	}

	public void sortCollection() {
		Collections.sort(students);
	}

	@Override
	public String toString() {
		return this.title;
	}

	public void printInfo() {
		System.out.println(this.toString());
		System.out.println("-----------------------------------------------");
		if (getNum() > 0) {
			System.out.println("�������� ������ " + getHead() + "\n");
			System.out.println("���������� ��������� � ������ " + getNum() + "\n");
		} else {
			System.out.println("� ������ ������ �� �������� ���������" + "\n");
		}
		if (averageMarkCalc() > 0) {
			System.out.println("������� ������ ������ \n" + String.format("%.2f", averageMarkCalc()) + "\n");
		}
	}

	public void printFullInfo() {
		System.out.println(this.toString());
		System.out.println("-----------------------------------------------");
		if (getNum() > 0) {
			System.out.println("�������� ������ " + getHead() + "\n");
			for (Student student : students) {
				student.printInfo();
			}
		} else {
			System.out.println("� ������ ������ �� �������� ���������" + "\n");
		}
		if (averageMarkCalc() > 0) {
			System.out.println("������� ������ ������ \n" + String.format("%.2f", averageMarkCalc()) + "\n");
		}
	}
}
