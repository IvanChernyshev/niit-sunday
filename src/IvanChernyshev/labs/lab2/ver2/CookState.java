
public class CookState extends States {

	public CookState(CoffeMachine coffe) {
		super(coffe);
	}

	@Override
	public void on() {
		resume = "������� ��� �������";
		printResume();
	}

	@Override
	public void off() {
		resume = "������ ��� ��������� �������, ����� ��������� ��������";
		printResume();
	}

	@Override
	public void coin(int cash) {
		resume = "���� ��� ��� ��������";
		printResume();
	}

	@Override
	public void choise() {
		resume = "������� ��� ������";
		printResume();
	}

	@Override
	public void check() {
		resume = "������ ��� ��������";
		printResume();
	}

	@Override
	public void cancel() {
		resume = "��������� � ������ ������ ������, ����������� ������ ��������";
		printResume();
	}

	@Override
	public void cook() {
		resume = "������������� ������� " + coffe.getMenu()[coffe.getIndexOfDrink()];
		printResume();
		coffe.setCurrentState(coffe.getFinishState());
	}

	@Override
	public void finish() {
		resume = "���� ������������� �������";
		printResume();
	}
}
