
public abstract class States {
	String resume;
	CoffeMachine coffe;

	public States(CoffeMachine coffe) {
		this.coffe = coffe;
	}

	public abstract void on();

	public abstract void off();

	public abstract void coin(int cash);

	public abstract void choise();

	public abstract void check();

	public abstract void cancel();

	public abstract void cook();

	public abstract void finish();

	public void printResume() {
		System.out.println(resume);
	}

	public String getResume() {
		return resume;
	}

	public void printState(CoffeMachine coffe) {
		System.out.println(coffe.getCookState());
	}

	@Override
	public String toString() {
		return States.class.getName();
	}
}
