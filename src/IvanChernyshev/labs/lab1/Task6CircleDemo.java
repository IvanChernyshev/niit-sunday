
public class Task6CircleDemo {
	public static void main(String[] args) {
		earthAndRope();
		System.out.println();
		System.out.println("------------------------------------------");
		System.out.println();
		pool();
	}

	static void earthAndRope() {
		Task6Circle circle = new Task6Circle();
		double radius = 6378100;
		System.out.println("������ �����: " + radius + " ������");
		circle.setRadius(radius);
		System.out.println("����� ���������� �����: " + String.format("%.2f", circle.getFerence()) + " ������");
		double newFerence = circle.getFerence() + 1;
		System.out.println("��������� ����� ����������: " + String.format("%.2f", newFerence) + " ������");
		circle.setFerence(newFerence);
		double result = circle.getRadius() - radius;
		System.out.println("����� ���������� ��: " + String.format("%.2f", result) + " ������");
	}

	static void pool() {
		Task6Circle circle = new Task6Circle();
		double poolRadius = 3;
		circle.setRadius(poolRadius);
		System.out.println("������ ��������: " + poolRadius + " �����");
		double poolArea = circle.getArea();
		System.out.println("������� ��������: " + String.format("%.2f", poolArea) + " ������");
		circle.setRadius(poolRadius + 1);
		double pathArea = circle.getArea() - poolArea;
		System.out.println("������� ������� ������ ��������: " + String.format("%.2f", pathArea) + " ������");
		double fenceLenght = circle.getFerence();
		System.out.println("����� ������ ������ ��������: " + String.format("%.2f", fenceLenght) + " ������");
		double priseOfPath = pathArea * 1000;
		double priseOfFence = fenceLenght * 2000;
		System.out.println("��������� ������� ������ ��������: " + String.format("%.2f", priseOfPath) + "������");
		System.out.println("��������� ������ ������ ��������: " + String.format("%.2f", priseOfFence) + " ������");
		System.out.println("�����: " + String.format("%.2f", priseOfPath + priseOfFence) + " ������");
	}
}
