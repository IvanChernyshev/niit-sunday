package education.lab1;

public class Task1Collatz {
	long longestSequence;
	long startNumber;

	public static void main(String[] args) {
		Task1Collatz collatz = new Task1Collatz();
		collatz.calculate();
	}

	public void calculate() {
		for (long i = 1; i < 1000000; i++) {
			calculate(i);
		}
		printLongestSequence();
	}

	private void calculate(long index) {
		long localNumber = index;
		long numberOfIterations = 0;
		do {
			if (isOddNumber(index)) {
				index = index * 3 + 1;
			} else {
				index = index / 2;
			}
			numberOfIterations++;
		} while (index != 1);
		// System.out.println(numberOfIterations + " последовательностей для
		// числа " + localNumber);
		compareOfResults(localNumber, numberOfIterations);
	}

	private void compareOfResults(long index, long iterations) {
		if (iterations > longestSequence) {
			this.startNumber = index;
			this.longestSequence = iterations;
		}
	}

	private void printLongestSequence() {
		System.out.println(longestSequence + " последовательностей для числа " + startNumber);
	}

	private boolean isOddNumber(long index) {
		return index % 2 != 0 ? true : false;
	}
}
