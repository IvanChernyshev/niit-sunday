
import java.util.*;

public class Task2SqrtDemoVer1 {
	public static void main(String[] args) {
		calculate();
	}

	static void calculate() {
		try {
			Task2SqrtVer1 sqrt = new Task2SqrtVer1();
			Scanner scanner = new Scanner(System.in);
			double number = getUserNumber(scanner);
			int scale = getUserScale(scanner);
			scanner.close();
			double result = sqrt.calc(number);
			printResult(number, result, scale);
		} catch (UnsupportedOperationException ex) {
			System.out.println("���������� ��� ���!");
		}
	}

	private static void printHeader() {
		System.out.println("��������� ����������� �����, ������� ��������");
	}

	private static void printScale() {
		System.out.println("���������� ��������:");
		System.out.println("1) �� ������");
		System.out.println("2) �� �����");
		System.out.println("3) �� ��������");
	}

	private static void printWarning() {
		System.out.println("����� ������� ��������� ��������");
	}

	private static void printResult(double number, double res, int scale) {
		String result = null;
		switch (scale) {
		case 1:
			result = String.format("%.0f", res);
			break;
		case 2:
			result = String.format("%.2f", res);
			break;
		case 3:
			result = String.format("%.4f", res);
			break;
		}
		System.out.println("���������� ������ ����� " + number + " = " + result);
	}

	private static double getUserNumber(Scanner scanner) {
		double number = 0;
		printHeader();
		try {
			number = scanner.nextDouble();
		} catch (InputMismatchException ex) {
			printWarning();
			scanner.close();
			throw new UnsupportedOperationException();
		}
		return number;
	}

	private static int getUserScale(Scanner scanner) {
		int scale = 0;
		printScale();
		try {
			scale = scanner.nextInt();
		} catch (InputMismatchException ex) {
			printWarning();
			scanner.close();
			throw new UnsupportedOperationException();
		}
		return scale;
	}
}
