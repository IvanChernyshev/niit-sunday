import java.util.Scanner;

public class Lab13 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String source = in.nextLine();

        String[] sourceArray = source.split(",");
        for (int i = 0; i < sourceArray.length; i++) {
            if (!sourceArray[i].contains("-")) {
                System.out.print(sourceArray[i]);
                if (i != sourceArray.length - 1) {
                    System.out.print(",");
                }
            } else {
                String[] numberRangeArray = sourceArray[i].split("-");
                int numFirst = Integer.valueOf(numberRangeArray[0]).intValue();
                int numLast = Integer.valueOf(numberRangeArray[1]).intValue();
                int[] newArray = new int[numLast - numFirst + 1];
                for (int j = 0; j <= numLast - numFirst; j++) {
                    newArray[j] = numFirst + j;
                    System.out.print(newArray[j]);
                    if ((i != sourceArray.length - 1) || (j != numLast - numFirst)) {
                        System.out.print(",");
                    }
                }

            }
        }

    }
}
