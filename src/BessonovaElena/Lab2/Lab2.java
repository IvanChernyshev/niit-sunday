
public class Lab2 {
    public static void main(String[] args) {
        Automata user = new Automata("OFF");//новый пользователь автомата

        System.out.println(user.printState());//проверка состояния автомата
        user.on();//включение автомата переход в состояние WAIT
        System.out.println(user.printState());
        user.printMenu();
        String[] a = user.printMenu();
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);//печать меню автомата
        }
        System.out.println(user.coin(10));//закладываем деньги
        System.out.println(user.coin(5));//добавляем деньги
        user.choise(1);//выбираем позицию в меню
        user.check();//проверяем достаточную сумму
        System.out.println(user.coin(10));//добавляем деньги
        user.check();//если сумма достаточна, переход в состояние COOK
        System.out.println("сдача " + user.change());//выдаем сдачу
        System.out.println(user.printState());
        user.cook();
        user.finish();//переход в состояние WAIT
        System.out.println(user.printState());
        user.off();//выключение (только из состояние WAIT)
        System.out.println(user.printState());

    }
}

class Automata {
    private int cash;
    private String[] menu = {"Caffe1", "Caffe2"};
    private int[] prices = {20, 30};
    private int cafePrice;

    private enum STATE {OFF, WAIT, ACCEPT, CHECK, COOK}

    ;

    private STATE state;

    Automata(String state) {
        this.state = STATE.valueOf(state);
    }

    STATE on() {
        if (this.state == STATE.OFF)
            return this.state = STATE.WAIT;
        else return this.state;
    }

    STATE off() {
        if (this.state == STATE.WAIT)
            return this.state = STATE.OFF;
        else return this.state;
    }

    int coin(int cash) {
        this.state = STATE.ACCEPT;
        return this.cash += cash;
    }

    int calcel() {
        this.state = STATE.WAIT;
        return this.cash;
    }

    int choise(int menuPosition) {
        this.state = STATE.CHECK;
        if ((menuPosition == 1) || (menuPosition == 2)) {
            switch (menuPosition) {
                case 1:
                    cafePrice = prices[0];
                    break;
                case 2:
                    cafePrice = prices[1];
                    break;
            }
        }
        return cafePrice;
    }

    boolean check() {
        if (this.cash < this.cafePrice) {
            this.state = STATE.ACCEPT;
            System.out.println("not enough");
            return false;
        } else {
            this.state = STATE.COOK;
            return true;
        }
    }

    int change() {
        if (this.cash > this.cafePrice) {
            return this.cash - this.cafePrice;
        } else return 0;
    }

    void cook() {
        if (this.state == STATE.COOK) {
            System.out.println(" ...cooking... ");
        } else {
            System.out.println(" not ready ");
        }
    }

    void finish() {
        this.state = STATE.WAIT;
    }

    String[] printMenu() {
        String[] caffePrice = new String[menu.length];
        for (int i = 0; i < this.menu.length; i++) {
            caffePrice[i] = i + 1 + " " + this.menu[i] + " " + this.prices[i];
        }
        return caffePrice;
    }

    String printState() {
        return this.state.toString();
    }


}
